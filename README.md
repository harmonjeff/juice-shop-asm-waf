# Juice Shop ASM WAF

## Description
A small demo GitLab project for storing F5 ASM policy and triggering an update of the policy on a BigIP when changes are committed.

## Helpful Resources

- [F5 ASM Declarative WAF](https://clouddocs.f5.com/products/waf-declarative-policy/v16_1.html)
- [F5 ASM 16.1 Schema Description](https://clouddocs.f5.com/products/waf-declarative-policy/declarative_policy_v16_1.html) (incomplete with only top level entities defined)

## Not A Model For a GitLab Pipeline
This repository is a basic example of how ASM policy can be stored remotely from a BigIP device and one way to use a GitLab pipeline to trigger the BigIP to update the ASM policy by pulling the JSON from the remote. The emphasis here is getting the BigIP to pull the policy JSON from the remote server on commit, not proper GitLab stages that include scans, linting, testing, and deployments to multiple environments.  <span style="color:red">This repository is for demonstration purposes only and MUST NOT be used for production purposes.</span>

***

## Important Notes
1. <u>JSON Content Profile</u>: Couldn't find a way to declaratively define a schema file for a JSON content profile in the ASM policy.  All of my attempts failed with an "Associated record for validation_files not found -- aborting" error.  This can be a very effective and important way to have JSON data validated by ASM policy.  It seems like the best way to do that might be to upload the json schema file to the BigIP at some location where ASM expects it and reference it.  I didn't try that method.  Here are the declarative methods I tried:

    - Specifying the `includeURL` without a `jsonValidationFile` object (which is not compliant with the schema).  Pointed the includeURL to the json schema files in this repository that are publicly available.  Nothing seemed to make the BigIP pull the JSON file.
    - Specifying the `jsonValidationFile` object with contents and filename and no `includeURL` (also not compliant with the schema)
    - Specifying both the `includeURL` and `jasonValidationFile` object (conforming to the schema).  The schema doesn't make sense to me.  To me it should be that an includeURL points the BigIP to where the schema file can be pulled from a remote repository OR a filename and file contents should be defined.  Still, conforming the ASM policy JSON to the schema had the same failed result.<br><br>

2. <u>Refresh ASM Policy</u>: The BigIP doesn't poll the remote location for updates to the ASM policy.  There are two options to trigger the BigIP to reach out and update the ASM policy stored remotely (like here in gitlab).  Both methods require that the "ingoreChanges" attribute is set to false as you declaratively define the application in AS3.

    1. Do an POST to AS3 declare with the full application partition defined.  Even if the AS3 JSON that defines the application has a mode of "selective", the BigIP will remove any element of the application that is not in the JSON when using POST.  This is very much on purpose as not supplying a piece of the application means you no longer need that piece and the BigIP destroys those objects.
    2. Do an AS3 declare PATCH (completely different JSON schema) that "replaces" the URL for the WAF policy.<br><br>

3. <u>ASM Policy Development</u>: The `juice_shop_waf.json` is the result of exporting the policy developed using the BigIP Management UI and then exported.  It is possible to develop the policy without using the F5 Management UI, but it is far slower.  My recommendation is to create the policy in a dev or test BigIP, using the learning features in particular, then export to JSON and store it in version control for deployment to other environments.

4. <u>ASM Exported Policy Linting Issue</u>: The export from the BigIP does not fully conform to the ASM WAF Schema (at least not v16.1). The attack signature IDs are strings when export out of the BigIP and the schema wants them as integer.  I didn't try fixing that in the file and it seems to work fine when applying the policy to another BigIP.

5. <u>ASM Exported Policy Structure Issue</U>: I never tested if a repeated export from the BigIP produced files that were the same, though I know that doing policy exports to XML in the past did not result in the ordering of the elements being the same even when no changes were made to the policy.  This can make it very hard to compare policy line-by-line.  This should be tested in JSON format just to make sure that is understood and not expected to be the same as far as the ordering of the elements.

***

## Installation
There are two dependencies for running this project.

1. A BigIP setup and ready to receive an HTTP PATCH telling it to refresh the ASM policy from this project

2. A gitlab-runner that provides a shell with Ansible installed and working to execute the playbook specified in the pipeline.
